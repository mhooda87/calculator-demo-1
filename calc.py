from flask import Flask, render_template, request
import os

app = Flask(__name__)
# app.config.from_object(__name__)

@app.route('/')
def welcome():
    return render_template('form.html')

@app.route('/', methods=['POST'])
def result():
    var_1 = request.form.get("var_1", type=int, default=0)
    var_2 = request.form.get("var_2", type=int, default=0)
    operation = request.form.get("operation")
    if(operation == 'Addition'):
        result = add(var_1,var_2)
    elif(operation == 'Subtraction'):
        result = subtract(var_1,var_2)
    elif(operation == 'Multiplication'):
        result = multiply(var_1,var_2)
    elif(operation == 'Division'):
    	result = divide(var_1,var_2)
    entry = result
    return render_template('form.html', entry=entry)

def add(x,y):
    return x + y


def multiply(x,y):
    return x * y


def subtract(x,y):
    return x - y


def divide(x,y):
	return x/y
if __name__ == '__main__':
	port = int(os.environ.get('PORT', 5001))
	app.run(debug=True, host='0.0.0.0', port=port)
